package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.ArrayList;

import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.utils.TestDataHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    public void testGetAllProducts() throws Exception {

        Pageable pageRequest = PageRequest.of(0, 5, Sort.Direction.ASC, "name");

        Mockito.when(this.productService.getProducts(pageRequest, null)).thenReturn(new PagedProductListDTO(new ArrayList<ProductDTO>() {
            {
                add(TestDataHelper.getProduct(1));
                add(TestDataHelper.getProduct(2));
                add(TestDataHelper.getProduct(3));
            }
        }, 1, 3L, 1));

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.results", Matchers.hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].productId", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].name", Is.is("Test product 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].description", Is.is("Test description 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].createdBy", Is.is("Test create person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].createdDateTime", Is.is("2018-08-24T01:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].modifiedBy", Is.is("Test modify person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].modifiedDateTime", Is.is("2018-08-24T01:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].quantityInStock", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[0].price", Is.is(11.11)))

                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].productId", Is.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].name", Is.is("Test product 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].description", Is.is("Test description 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].createdBy", Is.is("Test create person 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].createdDateTime", Is.is("2018-08-24T02:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].modifiedDateTime", Is.is("2018-08-24T02:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].modifiedBy", Is.is("Test modify person 2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].quantityInStock", Is.is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[1].price", Is.is(22.22)))

                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].productId", Is.is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].name", Is.is("Test product 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].description", Is.is("Test description 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].createdBy", Is.is("Test create person 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].createdDateTime", Is.is("2018-08-24T03:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].modifiedBy", Is.is("Test modify person 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].modifiedDateTime", Is.is("2018-08-24T03:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].quantityInStock", Is.is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.results[2].price", Is.is(33.33)))
                .andReturn();

    }

    @Test
    public void testGetOneProduct() throws Exception {

        Mockito.when(this.productService.getProduct(1L)).thenReturn(TestDataHelper.getProduct(1));

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Is.is("Test product 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", Is.is("Test description 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdBy", Is.is("Test create person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdDateTime", Is.is("2018-08-24T01:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedBy", Is.is("Test modify person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedDateTime", Is.is("2018-08-24T01:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantityInStock", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Is.is(11.11)))
                .andReturn();
    }

    @Test
    public void testUpdateProduct() throws Exception {

        ProductDTO product = TestDataHelper.getProduct(1);
        product.setDescription("A new test description");
        Mockito.when(this.productService.saveProduct(Mockito.any())).thenReturn(product);

        String productJson = new ObjectMapper().writeValueAsString(product);

        MockHttpServletRequestBuilder request =
                MockMvcRequestBuilders.put("/api/v1/products/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(productJson);

        MvcResult result = this.mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Is.is("Test product 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", Is.is("A new test description")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdBy", Is.is("Test create person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdDateTime", Is.is("2018-08-24T01:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedBy", Is.is("Test modify person 1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedDateTime", Is.is("2018-08-24T01:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantityInStock", Is.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Is.is(11.11)))
                .andReturn();

    }

    @Test
    public void testInsertProduct() throws Exception {

        ProductDTO product = TestDataHelper.getProduct(3);
        Mockito.when(this.productService.saveProduct(Mockito.any())).thenReturn(product);

        String productJson = new ObjectMapper().writeValueAsString(product);

        MockHttpServletRequestBuilder request =
                MockMvcRequestBuilders.post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(productJson);

        MvcResult result = this.mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId", Is.is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Is.is("Test product 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", Is.is("Test description 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdBy", Is.is("Test create person 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdDateTime", Is.is("2018-08-24T03:30:40")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedBy", Is.is("Test modify person 3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.modifiedDateTime", Is.is("2018-08-24T03:30:20")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantityInStock", Is.is(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Is.is(33.33)))
                .andReturn();

    }

    @Test
    public void testDeleteProduct() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/products/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }


}
