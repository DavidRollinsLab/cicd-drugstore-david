package com.abc.workshop.cicd.drugstore.controllers;

import static org.mockito.ArgumentMatchers.anyInt;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.service.ShoppingCartService;
import com.abc.workshop.cicd.drugstore.utils.TestDataHelper;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LandingPageControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ShoppingCartService shoppingCartService;


    @Before
    public void setUp() throws Exception {

        Mockito.when(this.productService.getProducts(anyInt(), anyInt())).thenReturn(new PagedProductListDTO(new ArrayList<ProductDTO>() {
            {
                add(TestDataHelper.getProduct(1));
                add(TestDataHelper.getProduct(2));
                add(TestDataHelper.getProduct(3));
            }
        }, 1, 3L, 1));

        Mockito.when(this.shoppingCartService.getProductsInCart()).thenReturn(new HashMap<ProductDTO, Integer>() {
            {
                put(TestDataHelper.getProduct(1), 1);
                put(TestDataHelper.getProduct(3), 5);
            }
        });

    }

    @Test
    public void verifiesHomePageLoads() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void verifiesHomePageHasData() throws Exception {

        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test description 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $11.11")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("In Stock: 1")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 2")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test description 2")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $22.22")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("In Stock: 2")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test product 3")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Test description 3")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Price: $33.33")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("In Stock: 3")))
                .andReturn();

        Assert.assertEquals(3, StringUtils.countMatches(result.getResponse().getContentAsString(), "Add to cart"));
    }

}
