package com.abc.workshop.cicd.drugstore.controllers;

import java.nio.charset.Charset;

import org.hamcrest.core.StringContains;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MyErrorControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void verifiesErrorPageLoads() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/error"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void verifiesErrorPageHasData() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/error"))
                .andExpect(MockMvcResultMatchers.content().contentType(new MediaType(MediaType.TEXT_HTML, Charset.forName("UTF-8"))))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Something went wrong!")))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString("Go Home")));
    }

}
