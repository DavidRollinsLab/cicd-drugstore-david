package com.abc.workshop.cicd.drugstore.e2e.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProductSection {

    WebElement productContainer;
    WebElement productLink;
    WebElement productDescription;

    public ProductSection(WebElement productContainer) {
        this.productContainer = productContainer;

        productLink = productContainer.findElement(By.cssSelector(".card-header > a"));
        productDescription = productContainer.findElement(By.cssSelector(".panel-body"));
    }

    public WebElement getPrice() {
        return productContainer.findElement(By.cssSelector("[data-node-se='price']"));
    }

    public WebElement getInStock() {
        return productContainer.findElement(By.cssSelector("[data-node-se='stock']"));
    }

    public WebElement getRemoveButton() {
        return productContainer.findElement(By.cssSelector("[data-node-se='remove-button']"));
    }

    public WebElement getAddButton() {
        return productContainer.findElement(By.cssSelector("[data-node-se='add-button']"));
    }

    public boolean isProductDisplayed() {
        return productContainer.isDisplayed();
    }

    public boolean isAddButtonDisplayed() {
        return getAddButton().isDisplayed();
    }

    public boolean isRemoveButtonDisplayed() {
        return getRemoveButton().isDisplayed();
    }

    public WebElement getQuantity() {
        return productContainer.findElement(By.cssSelector("[data-node-se='quantity']"));
    }

    public String getQuantityText() {
        return getQuantity().getText();
    }

    public String getPriceText() {
        return getPrice().getText();
    }

    public String getInStockText() {
        return getInStock().getText();
    }

    public void clickProductLink() {
        productLink.click();
    }

    public void clickAddButton() {
        getAddButton().click();
    }

    public void clickRemoveButton() {
        getRemoveButton().click();
    }

    public String getProductDescription() {
        return productDescription.getText();
    }
}
