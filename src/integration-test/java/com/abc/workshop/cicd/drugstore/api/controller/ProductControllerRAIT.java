package com.abc.workshop.cicd.drugstore.api.controller;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ProductControllerRAIT {

    @Value("${drugstore.url:http://localhost:8080}")
    private String serverAddress;

    @Value("${drugstore.useSeleniumHub:false}")
    private boolean useSeleniumHub;

    @Value("${drugstore.seleniumHubUrl:}")
    private String seleniumHubUrl;

    private static final Logger log = LoggerFactory.getLogger(ProductControllerRAIT.class);

    @Before
    public void setup() {
        RestAssured.baseURI = serverAddress;

        log.info("RestAssured.baseURI: {}", serverAddress);
    }

    @Test
    public void testGetAll() {


        RestAssured.when().request("GET", "/api/v1/products")
                .then()
                .statusCode(200)
                .assertThat()
                .body("results", Matchers.hasSize(5))
                .body("results.productId", Matchers.hasItems(7, 13, 24, 61, 60))
                .body("results[0].productId", Matchers.equalTo(7))
                .body("results[0].name", Matchers.equalTo("100 Days of Real Food: How We Did It, What We Learned, and 100 Easy, Wholesome Recipes Your Family Will Love"))
                .body("results[0].description", Matchers.equalTo(
                        "The creator of the 100 Days of Real Food blog draws from her hugely popular website to offer simple, affordable, family-friendly recipes and practical advice for eliminating processed foods from your family's diet."))
                .body("results[0].productCategory.productCategoryId", Matchers.equalTo(5))
                .body("results[0].productCategory.name", Matchers.equalTo("BOOKS"))
                .body("results[0].productCategory.createdDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("results[0].productCategory.createdBy", Matchers.equalTo("jdoe"))
                .body("results[0].productCategory.modifiedDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("results[0].productCategory.modifiedBy", Matchers.equalTo("jdoe"))
                .body("results[0].createdDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("results[0].createdBy", Matchers.equalTo("jdoe"))
                .body("results[0].modifiedDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("results[0].modifiedBy", Matchers.equalTo("jdoe"))
                .body("results[0].quantityInStock", Matchers.equalTo(3))
                .body("results[0].productCategoryId", Matchers.equalTo(5));

    }

    @Test
    public void testGetOneProduct() throws Exception {

        RestAssured.when().request("GET", "/api/v1/products/7")
                .then()
                .statusCode(200)
                .assertThat()
                .body("productId", Matchers.equalTo(7))
                .body("name", Matchers.equalTo("100 Days of Real Food: How We Did It, What We Learned, and 100 Easy, Wholesome Recipes Your Family Will Love"))
                .body("description", Matchers.equalTo(
                        "The creator of the 100 Days of Real Food blog draws from her hugely popular website to offer simple, affordable, family-friendly recipes and practical advice for eliminating processed foods from your family's diet."))
                .body("productCategory.productCategoryId", Matchers.equalTo(5))
                .body("productCategory.name", Matchers.equalTo("BOOKS"))
                .body("productCategory.createdDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("productCategory.createdBy", Matchers.equalTo("jdoe"))
                .body("productCategory.modifiedDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("productCategory.modifiedBy", Matchers.equalTo("jdoe"))
                .body("createdDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("createdBy", Matchers.equalTo("jdoe"))
                .body("modifiedDateTime", Matchers.equalTo("2016-09-01T08:00"))
                .body("modifiedBy", Matchers.equalTo("jdoe"))
                .body("quantityInStock", Matchers.equalTo(3))
                .body("productCategoryId", Matchers.equalTo(5));

    }

}
