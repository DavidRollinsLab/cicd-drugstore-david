package com.abc.workshop.cicd.drugstore.e2e;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import com.abc.workshop.cicd.drugstore.DrugStoreApplication;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public abstract class BaseSeleniumTest {

    private static final Logger log = LoggerFactory.getLogger(BaseSeleniumTest.class);

    @Value("${drugstore.url:http://localhost:8080}")
    private String serverAddress;

    @Value("${drugstore.useSeleniumHub:false}")
    private boolean useSeleniumHub;

    @Value("${drugstore.seleniumHubUrl:}")
    private String seleniumHubUrl;

    public WebDriver driver;

    public String baseURL;

    static {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() throws MalformedURLException {
        log.info("serverAddress: {}", serverAddress);

        RestAssured.baseURI = baseURL;

        baseURL = String.format("%s", serverAddress);
        log.info("BaseURL: {}", baseURL);

        log.info("useSeleniumHub: {} - url: {}", useSeleniumHub, seleniumHubUrl);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        if (useSeleniumHub) {
            driver = new RemoteWebDriver(new URL(seleniumHubUrl), options);
        } else {
            driver = new ChromeDriver();
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseURL);
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
