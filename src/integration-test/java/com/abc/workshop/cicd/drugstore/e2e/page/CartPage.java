package com.abc.workshop.cicd.drugstore.e2e.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {

    WebDriver driver;

    @FindBy(css = "[data-node-se='shopping-cart-page']")
    WebElement shoppingCartPage;

    WebElement productElement;

    ProductSection productSection;

    @FindBy(css = "[data-node-se='out-of-stock']")
    WebElement outOfStockMessage;

    @FindBy(css = "[data-node-se='total-amount']")
    WebElement totalAmountText;

    @FindBy(css = "[data-node-se='checkout-button']")
    WebElement checkoutButton;


    public CartPage(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }

    public boolean isOnCartPage() {
        return shoppingCartPage.isDisplayed();
    }

    public WebElement getProductElement() {
        return productElement;
    }

    public WebElement getOutOfStockMessage() {
        return outOfStockMessage;
    }

    public String getTotalAmountText() {
        return totalAmountText.getText();
    }

    public WebElement getCheckoutButton() {
        return checkoutButton;
    }

    public void expectProduct(String productId) {
        productElement = this.driver.findElement(By.cssSelector("[data-node-se=product-" + productId + "]"));
        productSection = new ProductSection(productElement);
    }

}
