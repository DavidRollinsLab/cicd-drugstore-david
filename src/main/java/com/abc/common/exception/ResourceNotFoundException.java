package com.abc.common.exception;

public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 359524453118512861L;

    public ResourceNotFoundException() {}

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(Throwable cause) {
        super(cause);
    }
}
