package com.abc.common.advice;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.abc.common.exception.BadRequestException;
import com.abc.common.exception.InternalServerErrorException;
import com.abc.common.exception.ResourceNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestErrorHandler.class);

    @ExceptionHandler({ResourceNotFoundException.class})
    public void handleResourceNotFoundException(ResourceNotFoundException e, HttpServletResponse response) throws IOException {
        LOGGER.error(this.formatLogMessage(e.getMessage()));
        response.sendError(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

    @ExceptionHandler({BadRequestException.class})
    public void handleBadRequestException(BadRequestException e, HttpServletResponse response) throws IOException {
        LOGGER.error(this.formatLogMessage(e.getMessage()));
        response.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler({InternalServerErrorException.class})
    void handleInternalServerErrorException(InternalServerErrorException e, HttpServletResponse response) throws IOException {
        LOGGER.error(this.formatLogMessage(e.getMessage()));
        e.printStackTrace();
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }

    private String formatLogMessage(String message) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\n===============================================================================================================\n");
        sb.append(message).append("\n");
        sb.append("===============================================================================================================\n\n");
        return sb.toString();
    }
}
