package com.abc.common.client;

import java.util.ArrayList;
import java.util.List;

import com.abc.common.interceptor.LoggingRequestInterceptor;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/*
 * The SlalomRestTemplate adds more capability than the default resttemplate Uses the
 * HttpComponentsClientHttpRequestFactory to support all REST Directives Has the Jaskson2 to convert
 * from JSON to and From POJOS Has the Form message converter to post forms Has the String Message
 * converter to be able read and write strings Has the LoggingRequest Interceptor to log requests
 * and responses
 */
public class SlalomRestTemplate extends RestTemplate {

    SlalomRestTemplate() {
        super(new HttpComponentsClientHttpRequestFactory());
        ClientHttpRequestInterceptor loggingRequestInterceptor = new LoggingRequestInterceptor();
        this.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        this.getMessageConverters().add(new StringHttpMessageConverter());
        this.getMessageConverters().add(new FormHttpMessageConverter());
        List<ClientHttpRequestInterceptor> arrayList = new ArrayList<ClientHttpRequestInterceptor>();
        arrayList.add(loggingRequestInterceptor);
        this.setInterceptors(arrayList);

    }
}
