package com.abc.workshop.cicd.drugstore.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FilterConditionDTO {
    @JsonProperty("Field")
    private String field;
    @JsonProperty("Op")
    private String operator;
    @JsonProperty("Data")
    private Long data;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }
}
