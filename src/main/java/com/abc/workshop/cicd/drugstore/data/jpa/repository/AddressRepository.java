package com.abc.workshop.cicd.drugstore.data.jpa.repository;

import java.util.List;

import com.abc.workshop.cicd.drugstore.data.jpa.entity.AddressEntity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring data JPA repository used to access address information from the database.
 */
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {
    List<AddressEntity> findByCustomerCustomerId(Long customerId);


}
