package com.abc.workshop.cicd.drugstore.data.jpa.repository;

import com.abc.workshop.cicd.drugstore.data.jpa.entity.ProductCategoryEntity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring data JPA repository used to access product category information from the database.
 */
public interface ProductCategoryRepository extends JpaRepository<ProductCategoryEntity, Long> {
}
