package com.abc.workshop.cicd.drugstore.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.OrderDAO;
import com.abc.workshop.cicd.drugstore.dto.OrderDTO;
import com.abc.workshop.cicd.drugstore.model.Order;
import com.abc.workshop.cicd.drugstore.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
    private static String ORDER_NOT_FOUND_MESSAGE = "Order not found for id: ";

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private MapperUtils mapper;

    public List<OrderDTO> getAllOrders(Long customerId) {
        List<Order> orders = orderDAO.getAllOrders(customerId);

        return orders.stream().map(order -> mapper.map(order, OrderDTO.class)).collect(Collectors.toList());
    }

    public OrderDTO getOrder(Long orderId) {
        Order order = orderDAO.getOrder(orderId);
        if (order == null) {
            throw new ResourceNotFoundException(ORDER_NOT_FOUND_MESSAGE + orderId);
        }

        return mapper.map(order, OrderDTO.class);
    }

    public OrderDTO saveOrder(OrderDTO request) {
        Order order = mapper.map(request, Order.class);
        orderDAO.saveOrder(order);

        return mapper.map(order, OrderDTO.class);
    }

    public void deleteOrder(Long orderId) {
        orderDAO.deleteOrder(orderId);
    }
}
