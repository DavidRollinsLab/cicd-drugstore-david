package com.abc.workshop.cicd.drugstore.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {
    Long productId;
    @NotNull
    private String name;
    private String description;
    private BigDecimal price;
    private ProductCategoryDTO productCategory;
    private LocalDateTime createdDateTime;
    private String createdBy;
    private LocalDateTime modifiedDateTime;
    private String modifiedBy;
    private Integer quantityInStock;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getProductCategoryId() {
        return this.productCategory != null ? this.productCategory.getProductCategoryId() : null;
    }

    public ProductCategoryDTO getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategoryDTO productCategory) {
        this.productCategory = productCategory;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    @JsonProperty("createdDateTime")
    public String getCreatedDateTimeAsString() {
        return createdDateTime.toString();
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getModifiedDateTime() {
        return modifiedDateTime;
    }

    @JsonProperty("modifiedDateTime")
    public String getModifiedDateTimeAsString() {
        return modifiedDateTime.toString();
    }

    public void setModifiedDateTime(LocalDateTime modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProductDTO.class.getSimpleName() + "[", "]").add("productId=" + productId).add("name='" + name + "'").add("description='" + description + "'")
                .add("price=" + price).add("productCategory=" + productCategory).add("createdDateTime=" + createdDateTime).add("createdBy='" + createdBy + "'")
                .add("modifiedDateTime=" + modifiedDateTime).add("modifiedBy='" + modifiedBy + "'").add("quantityInStock=" + quantityInStock).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ProductDTO that = (ProductDTO) o;
        return Objects.equals(getProductId(), that.getProductId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription())
                && Objects.equals(getPrice(), that.getPrice()) && Objects.equals(getProductCategory(), that.getProductCategory())
                && Objects.equals(getCreatedDateTime(), that.getCreatedDateTime()) && Objects.equals(getCreatedBy(), that.getCreatedBy())
                && Objects.equals(getModifiedDateTime(), that.getModifiedDateTime()) && Objects.equals(getModifiedBy(), that.getModifiedBy())
                && Objects.equals(getQuantityInStock(), that.getQuantityInStock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getName(), getDescription(), getPrice(), getProductCategory(), getCreatedDateTime(), getCreatedBy(), getModifiedDateTime(),
                getModifiedBy(), getQuantityInStock());
    }
}
