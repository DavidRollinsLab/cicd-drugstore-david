package com.abc.workshop.cicd.drugstore.dao.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.AddressDAO;
import com.abc.workshop.cicd.drugstore.data.jpa.entity.AddressEntity;
import com.abc.workshop.cicd.drugstore.data.jpa.repository.AddressRepository;
import com.abc.workshop.cicd.drugstore.model.Address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AddressDAOImpl implements AddressDAO {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MapperUtils mapper;

    public List<Address> getAllAddresses() {
        List<AddressEntity> addresses = addressRepository.findAll();

        return addresses.stream().map(address -> mapper.map(address, Address.class)).collect(Collectors.toList());
    }

    public Address getAddress(Long addressId) {
        Optional<AddressEntity> optionalAddressEntity = addressRepository.findById(addressId);

        return optionalAddressEntity.map(addressEntity -> mapper.map(addressEntity, Address.class))
                .orElseThrow(() -> new ResourceNotFoundException("Address not found: " + addressId));
    }

    public Address saveAddress(Address address) {
        AddressEntity addressEntity = mapper.map(address, AddressEntity.class);

        return mapper.map(addressRepository.save(addressEntity), Address.class);
    }

    public void deleteAddress(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    public List<Address> getAllAddressesForCustomer(Long customerId) {
        List<AddressEntity> addresses = addressRepository.findByCustomerCustomerId(customerId);

        return addresses.stream().map(address -> mapper.map(address, Address.class)).collect(Collectors.toList());
    }
}
