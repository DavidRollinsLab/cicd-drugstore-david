package com.abc.workshop.cicd.drugstore.data.jpa.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.abc.workshop.cicd.drugstore.data.jpa.utils.LocalDateTimeAttributeConverter;

@Entity
@Table(name = "orders")
public class OrderEntity implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long orderId;
    @Column(name = "ORDER_DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime orderDate;
    @Column(name = "SHIP_DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime shipDate;
    @Column(name = "LAST_MODIFIED_DATE")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime lastModifiedDate;
    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private CustomerEntity customer;
    @OneToMany(mappedBy = "order")
    private List<OrderDetailEntity> orderDetails;
    @Column(name = "CREATED_DATE_TIME")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime createdDateTime;
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "MODIFIED_DATE_TIME")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime modifiedDateTime;
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateTime getShipDate() {
        return shipDate;
    }

    public void setShipDate(LocalDateTime shipDate) {
        this.shipDate = shipDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public List<OrderDetailEntity> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailEntity> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(LocalDateTime modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
