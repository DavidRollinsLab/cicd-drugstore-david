package com.abc.workshop.cicd.drugstore.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.workshop.cicd.drugstore.dao.ProductCategoryDAO;
import com.abc.workshop.cicd.drugstore.dto.ProductCategoryDTO;
import com.abc.workshop.cicd.drugstore.model.ProductCategory;
import com.abc.workshop.cicd.drugstore.service.ProductCategoryService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
    private static String PRODUCT_CATEGORY_NOT_FOUND_MESSAGE = "Product category not found for ID: ";
    @Autowired
    private ProductCategoryDAO productCategoryDAO;

    @Autowired
    private ModelMapper mapper;

    public List<ProductCategoryDTO> getProductCategories() {
        List<ProductCategory> productCategories = productCategoryDAO.getProductCategories();

        return productCategories.stream().map(productCategory -> mapper.map(productCategory, ProductCategoryDTO.class)).collect(Collectors.toList());
    }

    public ProductCategoryDTO getProductCategory(Long productCategoryId) {
        ProductCategory productCategory = productCategoryDAO.getProductCategory(productCategoryId);
        if (productCategory == null) {
            throw new ResourceNotFoundException(PRODUCT_CATEGORY_NOT_FOUND_MESSAGE + productCategoryId);
        }

        return mapper.map(productCategory, ProductCategoryDTO.class);
    }

    public ProductCategoryDTO saveProductCategory(ProductCategoryDTO request) {
        ProductCategory productCategory = mapper.map(request, ProductCategory.class);
        productCategoryDAO.saveProductCategory(productCategory);

        return mapper.map(productCategory, ProductCategoryDTO.class);
    }

    public void deleteProductCategory(Long productCategoryId) {
        productCategoryDAO.deleteProductCategory(productCategoryId);
    }

}
