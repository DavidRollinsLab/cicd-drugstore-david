package com.abc.workshop.cicd.drugstore.service;

import java.util.Map;
import java.util.Set;

import com.abc.workshop.cicd.drugstore.dto.FilterGroupDTO;
import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.exception.NotEnoughProductsInStockException;

import org.springframework.data.domain.Pageable;

public interface ProductService {

    /**
     *
     * @param page 0 based
     * @param size
     * @return
     */
    PagedProductListDTO getProducts(int page, int size);

    /**
     * Returns all the product objects.
     *
     * @param pageRequest Request object used for paging and sorting
     * @param filters Object containing query filters
     * @return The list of product objects
     */
    PagedProductListDTO getProducts(Pageable pageRequest, FilterGroupDTO filters);


    PagedProductListDTO getProducts();

    /**
     * Returns the product object for the specified product ID if found.
     *
     * @param productId The product ID
     * @return The product object
     */
    ProductDTO getProduct(Long productId);

    /**
     * Saves a product and returns it
     *
     * @param productDTO The product information
     * @return The productDTO
     */
    ProductDTO saveProduct(ProductDTO productDTO);

    Set<ProductDTO> saveProducts(Set<ProductDTO> products);

    /**
     * Deletes an existing product.
     *
     * @param productId The ID of the product to delete
     */
    void deleteProduct(Long productId);

}
