package com.abc.workshop.cicd.drugstore.api.controller;

import java.io.IOException;

import javax.validation.Valid;

import com.abc.workshop.cicd.drugstore.dto.FilterGroupDTO;
import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/v1/products"})
public class ProductController {
    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    /**
     * Returns all product response objects.
     *
     *
     * @return The list of product resource
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PagedProductListDTO> getAll(@RequestParam(required = false) String sortColumn, @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) Integer pageNumber, @RequestParam(required = false) String filters) {

        if (sortColumn == null) {
            sortColumn = "name";
        }

        if (pageSize == null) {
            pageSize = 5;
        }

        if (pageNumber == null) {
            pageNumber = 1;
        }

        Pageable pageRequest = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, sortColumn);
        return new ResponseEntity<>(productService.getProducts(pageRequest, this.transform(filters)), HttpStatus.OK);
    }

    /**
     * Returns a product resource if found using the specified product id.
     *
     * @param productId The product ID
     * @return The product resource
     */
    @RequestMapping(value = "/{productId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> getProduct(@PathVariable Long productId) {
        return new ResponseEntity<>(productService.getProduct(productId), HttpStatus.OK);
    }

    /**
     * Creates a new product resource.
     *
     * @return The create response
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> createProduct(@RequestBody @Valid ProductDTO request) {
        return new ResponseEntity<>(productService.saveProduct(request), HttpStatus.CREATED);
    }

    /**
     * Updates an existing product resource.
     *
     * @return The update response
     */
    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long productId, @RequestBody @Valid ProductDTO request) {
        return new ResponseEntity<>(productService.saveProduct(request), HttpStatus.OK);
    }

    /**
     * Deletes an existing product resource.
     *
     * @param productId The product ID
     * @return The delete response
     */
    @RequestMapping(value = "/{productId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDTO> deleteProduct(@PathVariable Long productId) {
        productService.deleteProduct(productId);

        return new ResponseEntity(HttpStatus.OK);
    }

    private FilterGroupDTO transform(String filters) {
        FilterGroupDTO filterGroupDTO = null;

        if (filters != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                filterGroupDTO = mapper.readValue(filters, FilterGroupDTO.class);
            } catch (IOException ioe) {
                LOG.warn("Failed to convert filter string to object");
            }
        }

        return filterGroupDTO;
    }

}
