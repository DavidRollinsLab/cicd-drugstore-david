package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.List;

import javax.validation.Valid;

import com.abc.workshop.cicd.drugstore.dto.ProductCategoryDTO;
import com.abc.workshop.cicd.drugstore.service.ProductCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller used to create, retrieve, update, and delete product categories.
 */
@RestController
@RequestMapping({"/api/v1/productCategories"})
public class ProductCategoryController {
    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * Returns all product category response objects.
     *
     * @return The list of product resource
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductCategoryDTO>> getAll() {
        return new ResponseEntity<>(productCategoryService.getProductCategories(), HttpStatus.OK);
    }

    /**
     * Returns a product category resource if found using the specified ID.
     *
     * @param productCategoryId The product category ID
     * @return The product category resource
     */
    @RequestMapping(value = "/{productCategoryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductCategoryDTO> getProductCategory(@PathVariable Long productCategoryId) {
        return new ResponseEntity<>(productCategoryService.getProductCategory(productCategoryId), HttpStatus.OK);
    }

    /**
     * Creates a new product category resource.
     *
     * @return The create response
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductCategoryDTO> createProductCategory(@RequestBody @Valid ProductCategoryDTO request) {
        return new ResponseEntity<>(productCategoryService.saveProductCategory(request), HttpStatus.CREATED);
    }

    /**
     * Updates an existing product category resource.
     *
     * @return The update response
     */
    @RequestMapping(value = "/{productCategoryId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductCategoryDTO> updateProductCategory(@PathVariable Long productCategoryId, @RequestBody @Valid ProductCategoryDTO request) {
        return new ResponseEntity<>(productCategoryService.saveProductCategory(request), HttpStatus.OK);
    }

    /**
     * Deletes an existing product category resource.
     *
     * @param productCategoryId The product category ID
     * @return The delete response
     */
    @RequestMapping(value = "/{productCategoryId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductCategoryDTO> deleteProductCategory(@PathVariable Long productCategoryId) {
        productCategoryService.deleteProductCategory(productCategoryId);

        return new ResponseEntity(HttpStatus.OK);
    }
}
