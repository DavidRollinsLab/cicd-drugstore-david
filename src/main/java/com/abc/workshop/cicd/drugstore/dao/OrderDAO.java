package com.abc.workshop.cicd.drugstore.dao;

import java.util.List;

import com.abc.workshop.cicd.drugstore.model.Order;

/**
 * Data Access Object for performing order-related operations.
 */
public interface OrderDAO {
    /**
     * Returns the list of all order objects for a customer.
     *
     * @param customerId the customer id
     * @return the list of order objects for a given customer
     */
    List<Order> getAllOrders(Long customerId);

    /**
     * Returns the order object for the specified order ID, if found.
     *
     * @param orderId the order ID
     * @return the order object
     */
    Order getOrder(Long orderId);

    /**
     * Saves order info.
     *
     * @param order the order information
     * @return the updated order object
     */
    Order saveOrder(Order order);

    /**
     * Deletes an existing order.
     *
     * @param orderId the ID of the order to delete
     */
    void deleteOrder(Long orderId);
}
