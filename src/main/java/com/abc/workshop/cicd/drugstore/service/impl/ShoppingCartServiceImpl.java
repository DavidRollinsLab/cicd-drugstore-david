package com.abc.workshop.cicd.drugstore.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.exception.NotEnoughProductsInStockException;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.service.ShoppingCartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

/**
 * Shopping Cart is implemented with a Map, and as a session bean
 *
 * @author Dusan
 */
@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ProductService productService;

    private Map<ProductDTO, Integer> products = new HashMap<>();

    /**
     * If product is in the map just increment quantity by 1.
     * If product is not in the map with, add it with quantity 1
     *
     * @param product
     */
    @Override
    public void addProduct(ProductDTO product) {
        if (products.containsKey(product)) {
            products.replace(product, products.get(product) + 1);
        } else {
            products.put(product, 1);
        }
    }

    /**
     * If product is in the map with quantity > 1, just decrement quantity by 1.
     * If product is in the map with quantity 1, remove it from map
     *
     * @param product
     */
    @Override
    public void removeProduct(ProductDTO product) {
        if (products.containsKey(product)) {
            if (products.get(product) > 1)
                products.replace(product, products.get(product) - 1);
            else if (products.get(product) == 1) {
                products.remove(product);
            }
        }
    }

    /**
     * @return unmodifiable copy of the map
     */
    @Override
    public Map<ProductDTO, Integer> getProductsInCart() {
        return Collections.unmodifiableMap(products);
    }

    /**
     * Checkout will rollback if there is not enough of some product in stock
     *
     * @throws NotEnoughProductsInStockException
     */
    @Override
    public void checkout() throws NotEnoughProductsInStockException {
        ProductDTO product;
        for (Map.Entry<ProductDTO, Integer> entry : products.entrySet()) {
            // Refresh quantity for every product before checking
            product = productService.getProduct(entry.getKey().getProductId());
            if (product.getQuantityInStock() < entry.getValue())
                throw new NotEnoughProductsInStockException(product);
            entry.getKey().setQuantityInStock(product.getQuantityInStock() - entry.getValue());
        }
        productService.saveProducts(products.keySet());
        products.clear();
    }

    @Override
    public BigDecimal getTotal() {
        return products.entrySet().stream().map(entry -> entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }
}
