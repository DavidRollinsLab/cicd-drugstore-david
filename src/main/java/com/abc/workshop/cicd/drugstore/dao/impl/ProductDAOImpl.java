package com.abc.workshop.cicd.drugstore.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.ProductDAO;
import com.abc.workshop.cicd.drugstore.data.jpa.entity.ProductEntity;
import com.abc.workshop.cicd.drugstore.data.jpa.repository.ProductRepository;
import com.abc.workshop.cicd.drugstore.model.FilterCondition;
import com.abc.workshop.cicd.drugstore.model.FilterGroup;
import com.abc.workshop.cicd.drugstore.model.PagedProductList;
import com.abc.workshop.cicd.drugstore.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAOImpl implements ProductDAO {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MapperUtils mapper;

    public PagedProductList getProducts(Pageable pageRequest, FilterGroup filters) {
        Page<ProductEntity> productEntities = null;
        if (filters != null) {
            List<Long> filterList = new ArrayList();
            for (FilterCondition condition : filters.getConditions()) {
                if (condition.getField().equals("productCategoryId")) {
                    filterList.add(condition.getData());
                }
            }
            productEntities = productRepository.findByProductCategoryProductCategoryIdIn(filterList, pageRequest);
        } else {
            productEntities = productRepository.findAll(pageRequest);
        }

        List<Product> products = productEntities.getContent().stream().map(product -> mapper.map(product, Product.class)).collect(Collectors.toList());
        double totalPages = (double) productEntities.getTotalElements() / (double) pageRequest.getPageSize();
        return new PagedProductList(products, (int) Math.ceil(totalPages), productEntities.getTotalElements(), pageRequest.getPageNumber() + 1);
    }

    public Product getProduct(Long productId) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(productId);

        return optionalProductEntity.map(productEntity -> mapper.map(productEntity, Product.class))
                .orElseThrow(() -> new ResourceNotFoundException("Product not found: " + productId));
    }

    public Product saveProduct(Product product) {
        ProductEntity productEntity = mapper.map(product, ProductEntity.class);

        return mapper.map(productRepository.save(productEntity), Product.class);
    }

    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }
}
