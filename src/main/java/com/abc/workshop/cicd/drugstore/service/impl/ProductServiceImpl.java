package com.abc.workshop.cicd.drugstore.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.workshop.cicd.drugstore.dao.ProductDAO;
import com.abc.workshop.cicd.drugstore.dto.FilterGroupDTO;
import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.model.FilterGroup;
import com.abc.workshop.cicd.drugstore.model.PagedProductList;
import com.abc.workshop.cicd.drugstore.model.Product;
import com.abc.workshop.cicd.drugstore.service.ProductService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {
    private static String PRODUCT_NOT_FOUND_MESSAGE = "Product not found for id: ";

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ModelMapper modelMapper;


    /**
     *
     * @param page 0 based
     * @param size
     * @return
     */
    public PagedProductListDTO getProducts(int page, int size) {
        Pageable pageRequest = PageRequest.of(page, size);
        FilterGroupDTO filters = null;


        return getProducts(pageRequest, filters);
    }

    public PagedProductListDTO getProducts() {
        Pageable pageRequest = PageRequest.of(0, 10);
        FilterGroupDTO filters = null;


        return getProducts(pageRequest, filters);
    }

    public PagedProductListDTO getProducts(Pageable pageRequest, FilterGroupDTO filters) {
        FilterGroup filterGroup = (filters == null ? null : modelMapper.map(filters, FilterGroup.class));
        PagedProductList pagedProductList = productDAO.getProducts(pageRequest, filterGroup);
        List<ProductDTO> products = pagedProductList.getResults().stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
        return new PagedProductListDTO(products, pagedProductList.getTotalPages(), pagedProductList.getTotalRecords(), pagedProductList.getPageNumber());
    }

    public ProductDTO getProduct(Long productId) {
        Product product = productDAO.getProduct(productId);

        if (product == null) {
            throw new ResourceNotFoundException(PRODUCT_NOT_FOUND_MESSAGE + productId);
        }

        return this.modelMapper.map(product, ProductDTO.class);
    }

    @Transactional
    public ProductDTO saveProduct(ProductDTO request) {
        Product product = modelMapper.map(request, Product.class);
        validateProductExists(request);

        productDAO.saveProduct(product);

        return modelMapper.map(product, ProductDTO.class);
    }

    @Transactional
    public Set<ProductDTO> saveProducts(Set<ProductDTO> products) {
        return products.stream().map(productDTO -> saveProduct(productDTO)).collect(Collectors.toSet());
    }

    public void deleteProduct(Long productId) {
        productDAO.deleteProduct(productId);
    }

    private boolean validateProductExists(ProductDTO request) {
        if (request.getProductId() != null) {
            ProductDTO existingProduct = getProduct(request.getProductId());
            if (existingProduct == null) {
                throw new ResourceNotFoundException(PRODUCT_NOT_FOUND_MESSAGE + request.getProductId());
            }
        }

        return true;
    }

}
