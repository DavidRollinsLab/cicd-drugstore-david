package com.abc.workshop.cicd.drugstore.data.jpa.repository;

import com.abc.workshop.cicd.drugstore.data.jpa.entity.CustomerEntity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring data JPA repository used to access customer information from the database.
 */
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {
}
