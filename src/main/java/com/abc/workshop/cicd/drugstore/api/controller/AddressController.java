package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.List;

import javax.validation.Valid;

import com.abc.workshop.cicd.drugstore.dto.AddressDTO;
import com.abc.workshop.cicd.drugstore.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller used to create, retrieve, update, and delete addresses.
 */
@RestController
@RequestMapping({"/api/v1/addresses"})
public class AddressController {
    @Autowired
    AddressService addressService;

    /**
     * Updates an existing address resource.
     * @param request The updated address
     * @return The updated response
     */
    @RequestMapping(value = "/{addressId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressDTO> updateAddress(@PathVariable Long addressId, @RequestBody @Valid AddressDTO request) {
        return new ResponseEntity<>(addressService.saveAddress(request), HttpStatus.OK);
    }

    /**
     * Returns a list of all address resources.
     * @return The list of all addresses.
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AddressDTO>> getAllAddresses() {
        return new ResponseEntity<>(addressService.getAllAddresses(), HttpStatus.OK);
    }

    /**
     * Returns an address resource for the specified address id if it exists.
     * @param addressId The address ID
     * @return The address resource
     */
    @RequestMapping(value = "/{addressId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressDTO> getAddress(@PathVariable Long addressId) {
        return new ResponseEntity<>(addressService.getAddress(addressId), HttpStatus.OK);
    }

    /**
     * Creates a new address resource.
     * @param request The new address resource
     * @return The create response
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressDTO> createAddress(@RequestBody @Valid AddressDTO request) {
        return new ResponseEntity<>(addressService.saveAddress(request), HttpStatus.CREATED);
    }

    /**
     * Deletes an existing address resource.
     * @param addressId The address ID
     * @return The delete response.
     */
    @RequestMapping(value = "/{addressId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressDTO> deleteAddress(@PathVariable Long addressId) {
        addressService.deleteAddress(addressId);

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Returns list of all address resources for a specific Customer.
     * @param customerId The customer ID
     * @return The list of all addresses for that customer.
     */
    @RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AddressDTO>> getAllAddressesForCustomer(@PathVariable Long customerId) {
        return new ResponseEntity<>(addressService.getAllAddressesForCustomer(customerId), HttpStatus.OK);
    }
}
