package com.abc.workshop.cicd.drugstore.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.AddressDAO;
import com.abc.workshop.cicd.drugstore.dto.AddressDTO;
import com.abc.workshop.cicd.drugstore.model.Address;
import com.abc.workshop.cicd.drugstore.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {
    private static String ADDRESS_NOT_FOUND_MESSAGE = "Address not found for id: ";

    @Autowired
    private AddressDAO addressDAO;

    @Autowired
    private MapperUtils mapper;

    public List<AddressDTO> getAllAddresses() {
        List<Address> addresses = addressDAO.getAllAddresses();

        return addresses.stream().map(address -> mapper.map(address, AddressDTO.class)).collect(Collectors.toList());
    }

    public AddressDTO getAddress(Long addressId) {
        Address address = addressDAO.getAddress(addressId);
        if (address == null) {
            throw new ResourceNotFoundException(ADDRESS_NOT_FOUND_MESSAGE + addressId);
        }

        return this.mapper.map(address, AddressDTO.class);
    }

    public AddressDTO saveAddress(AddressDTO request) {
        Address address = mapper.map(request, Address.class);

        addressDAO.saveAddress(address);

        return mapper.map(address, AddressDTO.class);
    }

    public void deleteAddress(Long addressId) {
        addressDAO.deleteAddress(addressId);
    }

    public List<AddressDTO> getAllAddressesForCustomer(Long customerId) {
        List<Address> addresses = addressDAO.getAllAddressesForCustomer(customerId);

        return addresses.stream().map(address -> mapper.map(address, AddressDTO.class)).collect(Collectors.toList());
    }
}
