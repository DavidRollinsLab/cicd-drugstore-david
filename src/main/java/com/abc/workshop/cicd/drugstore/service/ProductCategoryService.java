package com.abc.workshop.cicd.drugstore.service;

import java.util.List;

import com.abc.workshop.cicd.drugstore.dto.ProductCategoryDTO;

public interface ProductCategoryService {
    /**
     * Returns all the product category objects.
     *
     * @return The list of product category objects
     */
    List<ProductCategoryDTO> getProductCategories();

    /**
     * Returns the product category object for the specified product category ID if found.
     *
     * @param productCategoryId the product category ID
     * @return the product category object
     */
    ProductCategoryDTO getProductCategory(Long productCategoryId);

    /**
     * Saves a product category and returns it.
     *
     * @param productCategory the product category information
     * @return the product category object
     */
    ProductCategoryDTO saveProductCategory(ProductCategoryDTO productCategory);

    /**
     * Deletes an existing product category.
     *
     * @param productCategoryId the ID of the product category to delete.
     */
    void deleteProductCategory(Long productCategoryId);
}
