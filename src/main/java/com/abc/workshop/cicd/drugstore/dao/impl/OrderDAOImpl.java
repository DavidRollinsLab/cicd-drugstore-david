package com.abc.workshop.cicd.drugstore.dao.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.OrderDAO;
import com.abc.workshop.cicd.drugstore.data.jpa.entity.OrderEntity;
import com.abc.workshop.cicd.drugstore.data.jpa.repository.OrderRepository;
import com.abc.workshop.cicd.drugstore.model.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderDAOImpl implements OrderDAO {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MapperUtils mapper;

    public List<Order> getAllOrders(Long customerId) {
        List<OrderEntity> orders = orderRepository.findByCustomerCustomerId(customerId);

        return orders.stream().map(order -> mapper.map(order, Order.class)).collect(Collectors.toList());
    }

    public Order getOrder(Long orderId) {
        Optional<OrderEntity> optionalOrderEntity = orderRepository.findById(orderId);

        return optionalOrderEntity.map(orderEntity -> mapper.map(orderEntity, Order.class)).orElseThrow(() -> new ResourceNotFoundException("Order not found: " + orderId));
    }

    public Order saveOrder(Order order) {
        OrderEntity orderEntity = mapper.map(order, OrderEntity.class);

        return mapper.map(orderRepository.save(orderEntity), Order.class);
    }

    public void deleteOrder(Long orderId) {
        orderRepository.deleteById(orderId);
    }
}
