package com.abc.workshop.cicd.drugstore.data.jpa.repository;

import java.util.List;

import com.abc.workshop.cicd.drugstore.data.jpa.entity.OrderEntity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring data JPA repository used to access order information from the database.
 */
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    List<OrderEntity> findByCustomerCustomerId(Long customerId);
}
