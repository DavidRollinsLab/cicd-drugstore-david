package com.abc.workshop.cicd.drugstore.service;

import java.math.BigDecimal;
import java.util.Map;

import com.abc.workshop.cicd.drugstore.dto.ProductDTO;
import com.abc.workshop.cicd.drugstore.exception.NotEnoughProductsInStockException;

public interface ShoppingCartService {

    void addProduct(ProductDTO product);

    void removeProduct(ProductDTO product);

    Map<ProductDTO, Integer> getProductsInCart();

    void checkout() throws NotEnoughProductsInStockException;

    BigDecimal getTotal();
}
