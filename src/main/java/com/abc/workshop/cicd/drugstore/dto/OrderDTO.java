package com.abc.workshop.cicd.drugstore.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderDTO {
    private Long orderId;
    private LocalDateTime orderDate;
    private LocalDateTime shipDate;
    private LocalDateTime lastModifiedDate;
    private CustomerDTO customer;
    private List<OrderDetailDTO> orderDetails;
    private LocalDateTime createdDateTime;
    private String createdBy;
    private LocalDateTime modifiedDateTime;
    private String modifiedBy;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    @JsonProperty("orderDate")
    public String getOrderDateTimeAsString() {
        return orderDate.toString();
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateTime getShipDate() {
        return shipDate;
    }

    @JsonProperty("shipDate")
    public String getShipDateTimeAsString() {
        return shipDate.toString();
    }

    public void setShipDate(LocalDateTime shipDate) {
        this.shipDate = shipDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    @JsonProperty("lastModifiedDate")
    public String getLastModifiedDateTimeAsString() {
        return lastModifiedDate.toString();
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getCustomerId() {
        return this.customer.getCustomerId();
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public List<OrderDetailDTO> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailDTO> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    @JsonProperty("createdDateTime")
    public String getCreatedDateTimeAsString() {
        return createdDateTime.toString();
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getModifiedDateTime() {
        return modifiedDateTime;
    }

    @JsonProperty("modifiedDateTime")
    public String getModifiedDateTimeAsString() {
        return modifiedDateTime.toString();
    }

    public void setModifiedDateTime(LocalDateTime modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
