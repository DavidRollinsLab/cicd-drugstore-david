package com.abc.workshop.cicd.drugstore.dao;

import java.util.List;

import com.abc.workshop.cicd.drugstore.model.ProductCategory;

/**
 * Data Access Object for performing product category-related operations.
 */
public interface ProductCategoryDAO {
    /**
     * Returns the list of all product category objects.
     *
     * @return the list of all product category objects
     */
    List<ProductCategory> getProductCategories();

    /**
     * Returns the product category object for the specified product category ID, if found.
     *
     * @param productCategoryId the product category ID
     * @return the product category object
     */
    ProductCategory getProductCategory(Long productCategoryId);

    /**
     * Saves product category info.
     *
     * @param productCategory the product category information
     * @return the updated product category object
     */
    ProductCategory saveProductCategory(ProductCategory productCategory);

    /**
     * Deletes an existing product category.
     *
     * @param productCategoryId the ID of the product category to delete
     */
    void deleteProductCategory(Long productCategoryId);
}
