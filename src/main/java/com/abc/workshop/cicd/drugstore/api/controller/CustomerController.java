package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.List;

import javax.validation.Valid;

import com.abc.workshop.cicd.drugstore.dto.CustomerDTO;
import com.abc.workshop.cicd.drugstore.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller used to create, retrieve, update, and delete customers.
 */
@RestController
@RequestMapping({"/api/v1/customers"})
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /**
     * Updates an existing customer resource.
     *
     * @return The update response
     */
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody @Valid CustomerDTO request) {
        return new ResponseEntity<CustomerDTO>(customerService.saveCustomer(request), HttpStatus.OK);
    }

    /**
     * Returns a list of all customer resources.
     *
     * @return The list of customers
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerDTO>> getAllCustomers() {
        return new ResponseEntity<List<CustomerDTO>>(customerService.getAllCustomers(), HttpStatus.OK);
    }

    /**
     * Returns a customer resource if found using the specified customer id.
     *
     * @param customerId The customer ID
     * @return The customer resource
     */
    @RequestMapping(value = "/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable Long customerId) {
        return new ResponseEntity<CustomerDTO>(customerService.getCustomer(customerId), HttpStatus.OK);
    }

    /**
     * Creates a new customer resource.
     *
     * @return The create response
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDTO> createCustomer(@RequestBody @Valid CustomerDTO request) {
        return new ResponseEntity<CustomerDTO>(customerService.saveCustomer(request), HttpStatus.CREATED);
    }

    /**
     * Deletes an existing customer resource.
     *
     * @param customerId The customer ID
     * @return The delete response
     */
    @RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDTO> deleteCustomer(@PathVariable Long customerId) {
        customerService.deleteCustomer(customerId);

        return new ResponseEntity(HttpStatus.OK);
    }
}
