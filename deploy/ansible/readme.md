# Connecting
go to ansible controller in Azure Portal and make sure you can access



```
ssh ssh ansible@ansible-controller.eastus2.cloudapp.azure.com
```


To copy current directory to ansible controller

```
 scp -r . ansible@ansible-controller.eastus2.cloudapp.azure.com:~
```