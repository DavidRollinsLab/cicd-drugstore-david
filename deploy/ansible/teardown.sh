#!/usr/bin/env bash
set -e
ansible-playbook -i lab-db.hosts database_delete.yaml
ansible-playbook -i lab-webapp.hosts webapp_delete.yaml
ansible-playbook -i lab-jenkins.hosts jenkins_delete.yaml
ansible-playbook -i lab-elasticsearch.hosts elasticsearch_delete.yaml
az aks delete -g selenium-group -n selenium-grid -y

