#!/usr/bin/env bash
until $(curl --output /dev/null --silent --head --fail http://104.208.218.56/actuator/health); do
    printf '.'
    sleep 5
done

./gradlew integrationTest \
    -Ddrugstore.url="http://104.208.218.56" \
    -Ddrugstore.useSeleniumHub=true \
    -Ddrugstore.seleniumHubUrl="http://104.45.158.13:4444/wd/hub" \
    --no-daemon \
    --stacktrace
