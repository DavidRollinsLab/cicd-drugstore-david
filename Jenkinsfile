pipeline {
    agent any

    triggers {
        pollSCM('*/2 * * * *')
    }

    environment {
        appServerName = 'app00'
        dbServerName = 'db00'
        userName = 'app_user'
        appServerURL = 'http://168.61.53.250'
        seleniumHubURL = 'http://104.45.158.13:4444/wd/hub'
        SPRING_PROFILES_ACTIVE = 'ci'
    }

    stages {
        stage('Checkout') {
            steps {
                deleteDir()
                script {
                    def scmVars = checkout scm
                    GIT_BRANCH = scmVars.GIT_BRANCH
                }
                stash name: 'checkout_stash'
            }
        }

        stage('Test') {
            environment {
                SPRING_PROFILES_ACTIVE = 'ci'
            }
            steps {
                sh './gradlew clean test -x integrationTest'
            }
        }

        stage('Build') {
            environment {
                SPRING_PROFILES_ACTIVE = 'dev'
            }
            steps {
                sh './gradlew clean assemble -x integrationTest'
            }
        }

        stage('Deploy') {
            steps {
                sh '''
                    ssh -o StrictHostKeyChecking=no -l ${userName} ${dbServerName} uname -a
                    ssh -o StrictHostKeyChecking=no -l ${userName} ${appServerName} uname -a

                    ssh -o StrictHostKeyChecking=no ${userName}@${appServerName} """
                     if (systemctl -q is-active drug-store.service)
                         then
                         echo "Stopping Application"
                         sudo systemctl stop drug-store.service
                     fi
                    """

                    ssh -o StrictHostKeyChecking=no ${userName}@${appServerName} "sudo mkdir -p /var/log/drug-store"

                    ssh -o StrictHostKeyChecking=no ${userName}@${appServerName} "rm -rf /home/${userName}/app && mkdir -p /home/${userName}/app"

                    scp build/libs/*.jar ${userName}@${appServerName}:/home/${userName}/app/drug-store.jar
                    scp deploy/service/drug-store.service ${userName}@${appServerName}:/home/${userName}/app/drug-store.service
                    scp deploy/service/drug-store.conf ${userName}@${appServerName}:/home/${userName}/app/drug-store.conf

                    ssh -o StrictHostKeyChecking=no ${userName}@${appServerName} "sudo ln -f -s /home/${userName}/app/drug-store.service /etc/systemd/system/drug-store.service"

                    ssh -o StrictHostKeyChecking=no ${userName}@${appServerName} "sudo systemctl enable drug-store.service && sudo systemctl restart drug-store.service"
                '''
            }
        }

        stage('Integration Test') {
            steps {
                sh '''
                    curl --connect-timeout 90 \
                         --max-time 5 \
                         --retry 10 \
                         --retry-delay 0 \
                         --retry-max-time 90 \
                        ${appServerURL}/actuator/health

                    ./gradlew test integrationTest \
                        -Ddrugstore.url="${appServerURL}" \
                        -Ddrugstore.useSeleniumHub=true \
                        -Ddrugstore.seleniumHubUrl="${seleniumHubURL}" \
                        --no-daemon \
                        --stacktrace

                '''
            }
        }
    }

    post {
        always {
            archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true

            junit 'build/test-results/**/*.xml'

            publishHTML (target: [
              allowMissing: false,
              alwaysLinkToLastBuild: false,
              keepAll: true,
              reportDir: 'build/reports/tests/test',
              reportFiles: 'index.html',
              reportName: "Unit Test Report"
            ])

            publishHTML (target: [
              allowMissing: false,
              alwaysLinkToLastBuild: false,
              keepAll: true,
              reportDir: 'build/integration-test-results',
              reportFiles: 'index.html',
              reportName: "Integration Test Report"
            ])
        }
    }
}