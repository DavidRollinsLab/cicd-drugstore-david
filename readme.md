# CI/CD Lab

### Prerequisites:

* Bitbucket Account - bitbucket.org
* Git command line - https://git-scm.com/downloads
* Java 8 or better - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


Optional:

* Source Tree - https://www.sourcetreeapp.com/






### Running application

For Mac:

```
./gradlew clean bootRun
```

For Windows:

```
gradlew.bat clean bootRun
```


URL: [http://localhost:8080/](http://localhost:8080/)



### Running unit tests:


```
./gradlew clean test -x integrationTest
```

### Running E2E tests

In two separate windows

In window 1
```
./gradlew clean bootRun
```

In window 2
```
./gradlew integrationTest
```


----
   dd